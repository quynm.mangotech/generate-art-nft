const fs = require("fs")

const width = 1000;
const height = 1000;
const dir = __dirname;
const description = "This is an NFT made by the coolest generative code.";
const baseImageUri = "https://hashlips/nft/";
const startEditonFrom = 1;
const endEditionAt = 10;
const editionSize = 10;
const rarityWeights = [
    {
        value: "super_rare",
        from: 1,
        to: 1
    },
    {
        value: "rare",
        from: 2,
        to: 4
    },
    {
        value: "original",
        from: 5,
        to: editionSize
    }

]


const cleanName = (_str) => {
    let name = _str.slice(0, -4);
    
    return name;
}

const getElements = (path) => {
    return fs
        .readdirSync(path)
        .filter((item) => !/(^|\/)\.[^\/\.]/g.test(item))
        .map((i) => {
            return {
                name: cleanName(i),
                path: `${path}/${i}`,
            }
        }) 
}

const layers = [
    {
        
        elements: {
            original: getElements(`${dir}/background/original`),
            rare: getElements(`${dir}/background/rare`),
            super_rare: getElements(`${dir}/background/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}
    },
    {
        elements: {
            original: getElements(`${dir}/body/original`),
            rare: getElements(`${dir}/body/rare`),
            super_rare: getElements(`${dir}/body/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}
    },
    {
        elements: {
            original: getElements(`${dir}/arms/original`),
            rare: getElements(`${dir}/arms/rare`),
            super_rare: getElements(`${dir}/arms/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}
    },
    {
        elements: {
            original: getElements(`${dir}/legs/original`),
            rare: getElements(`${dir}/legs/rare`),
            super_rare: getElements(`${dir}/legs/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}
    },
    {
        elements: {
            original: getElements(`${dir}/eyes/original`),
            rare: getElements(`${dir}/eyes/rare`),
            super_rare: getElements(`${dir}/eyes/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}
    },
    {
        elements: {
            original: getElements(`${dir}/mouth/original`),
            rare: getElements(`${dir}/mouth/rare`),
            super_rare: getElements(`${dir}/mouth/super_rare`)
        },
        position: {x: 0, y: 0},
        size: {width, height}  
    },
]

module.exports = { 
    layers, 
    width, 
    height, 
    description, 
    baseImageUri, 
    editionSize,  
    startEditonFrom, 
    endEditionAt, 
    rarityWeights
}